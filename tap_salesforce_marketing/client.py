"""REST client handling, including Salesforce-marketingStream base class."""

import requests
import re
from typing import Any, Dict, Optional

from singer_sdk.streams import RESTStream
from tap_salesforce_marketing.auth import OAuth2Authenticator
from pendulum import parse


class SalesforceMarketingStream(RESTStream):
    """Salesforce-marketing stream class."""

    @property
    def url_base(self) -> str:
        return self.config.get("rest_url")

    records_jsonpath = "$.items[*]"
    rep_key_name = None

    @property
    def authenticator(self) -> OAuth2Authenticator:
        sub_domain = self.config.get("rest_url")
        sub_domain = sub_domain.split(".")[0].split("//")[-1]
        oauth_url = f"https://{sub_domain}.auth.marketingcloudapis.com/v2/token"
        return OAuth2Authenticator(self, self.config, auth_endpoint=oauth_url)

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        previous_token = previous_token or 1
        res = response.json()
        if len(res.get("items")) == res.get("pageSize"):
            return previous_token + 1
        return None

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token

        start_date = self.get_starting_time(context)
        if self.replication_key and start_date and self.rep_key_name:
            start_date = start_date.strftime("%Y-%m-%dT%H:%M:%S")
            params[self.rep_key_name] = start_date
        return params
