"""Stream type classes for tap-salesforce-marketing."""

from singer_sdk import typing as th
from tap_salesforce_marketing.client import SalesforceMarketingStream


class CampaignsStream(SalesforceMarketingStream):
    """Define custom stream."""

    name = "campaigns"
    path = "/hub/v1/campaigns"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("createdDate", th.DateTimeType),
        th.Property("modifiedDate", th.DateTimeType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("campaignCode", th.DateTimeType),
        th.Property("color", th.DateTimeType),
        th.Property("favorite", th.BooleanType),
    ).to_dict()


class ApprovalItemsStream(SalesforceMarketingStream):
    """Define custom stream."""

    name = "approvals"
    path = "/hub/v1/approvals-v2"
    primary_keys = ["approvalItemId"]
    schema = th.PropertiesList(
        th.Property("approvalItemId", th.StringType),
        th.Property("objectType", th.StringType),
        th.Property("objectId", th.StringType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("workflowItemId", th.StringType),
        th.Property("workflowState", th.StringType),
        th.Property("workflowType", th.StringType),
        th.Property("workflowName", th.StringType),
        th.Property("deadline", th.StringType),
        th.Property(
            "commentCounts",
            th.ObjectType(
                th.Property("open", th.StringType),
                th.Property("addressed", th.StringType),
                th.Property("completed", th.StringType),
                th.Property("total", th.StringType),
            ),
        ),
        th.Property(
            "workflowItem",
            th.ObjectType(
                th.Property("workflowItemId", th.StringType),
                th.Property("workflowRound", th.StringType),
                th.Property("type", th.StringType),
                th.Property(
                    "currentState",
                    th.ObjectType(
                        th.Property("stateId", th.StringType),
                        th.Property("stateName", th.StringType),
                        th.Property("isEndState", th.StringType),
                        th.Property("modifiedDate", th.StringType),
                    ),
                ),
                th.Property(
                    "availableTransitions",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("id", th.StringType),
                            th.Property("name", th.StringType),
                            th.Property("isPersistable", th.StringType),
                        )
                    ),
                ),
                th.Property("blocked", th.StringType),
            ),
        ),
    ).to_dict()


class AssetCategoriesStream(SalesforceMarketingStream):
    """Define custom stream."""

    name = "asset_categories"
    path = "/asset/v1/content/categories"
    primary_keys = ["Id"]
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("description", th.StringType),
        th.Property("name", th.StringType),
        th.Property("parentId", th.IntegerType),
        th.Property("categoryType", th.StringType),
        th.Property("memberId", th.IntegerType),
        th.Property("enterpriseId", th.IntegerType),
        th.Property(
            "enterpriseId",
            th.ObjectType(
                th.Property("sharingType", th.StringType),
                th.Property("sharedWith", th.ArrayType(th.IntegerType)),
            ),
        ),
    ).to_dict()


class AuditEventsStream(SalesforceMarketingStream):
    """Define custom stream."""

    name = "audit_events"
    path = "/data/v1/audit/auditEvents"
    primary_keys = ["Id"]
    replication_key = "createdDate"
    rep_key_name = "start_date"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("createdDate", th.DateTimeType),
        th.Property("memberId", th.IntegerType),
        th.Property("enterpriseId", th.IntegerType),
        th.Property(
            "employee",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("employeeName", th.StringType),
                th.Property("userName", th.StringType),
            ),
        ),
        th.Property(
            "objectType",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property(
            "operation",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property(
            "object",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property("transactionId", th.StringType),
    ).to_dict()
